# Release 1.0.0
Initial release with support/wrappers for:

  - X.509 Certificate
  - OCSP Response
  - Key Management
  - Generate Private Key (KeyPair)
  - ECDH Shared Secret computation
  - Verify BrainpoolP256r1 signature
  - PACE protocol map Nonce for Shared Secret derivation
  - MAC calculation


