// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name NFCCardReaderProvider
import CardReaderProviderApi
import Combine
import CoreNFC
import DataKit
import Foundation
import GemCommonsKit
import HealthCardAccess
import Swift
public protocol NFCCardSession {
  func updateAlert(message: Swift.String)
  func invalidateSession(with error: Swift.String?)
  var card: CardReaderProviderApi.CardType { get }
}
extension NFCTagReaderSession {
  public enum Error : Swift.Error {
    case couldNotInitializeSession
    case unsupportedTag
    case nfcTag(error: Swift.Error)
    case userCancelled(error: Swift.Error)
  }
  public struct Publisher : Combine.Publisher {
    public typealias Output = NFCCardReaderProvider.NFCCardSession
    public typealias Failure = CoreNFC.NFCTagReaderSession.Error
    public func receive<S>(subscriber: S) where S : Combine.Subscriber, S.Failure == CoreNFC.NFCTagReaderSession.Publisher.Failure, S.Input == CoreNFC.NFCTagReaderSession.Publisher.Output
  }
  public struct Messages {
    public let discoveryMessage: Swift.String
    public let connectMessage: Swift.String
    public let noCardMessage: Swift.String
    public let multipleCardsMessage: Swift.String
    public let unsupportedCardMessage: Swift.String
    public let connectionErrorMessage: Swift.String
    public init(discoveryMessage: Swift.String, connectMessage: Swift.String, noCardMessage: Swift.String, multipleCardsMessage: Swift.String, unsupportedCardMessage: Swift.String, connectionErrorMessage: Swift.String)
  }
  public static func publisher(for pollingOption: CoreNFC.NFCTagReaderSession.PollingOption = .iso14443, on queue: Dispatch.DispatchQueue = .global(qos: .userInitiated), messages: CoreNFC.NFCTagReaderSession.Messages) -> CoreNFC.NFCTagReaderSession.Publisher
}
