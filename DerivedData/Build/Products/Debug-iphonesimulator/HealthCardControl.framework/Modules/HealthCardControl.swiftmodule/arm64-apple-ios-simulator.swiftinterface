// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target arm64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name HealthCardControl
import ASN1Kit
import CardReaderProviderApi
import Combine
import CommonCrypto
import Foundation
import GemCommonsKit
import HealthCardAccess
import OpenSSL
import Security
import Swift
public enum CardAid : HealthCardAccess.ApplicationIdentifier {
  case egk
  case hba
  case smcb
  public func hash(into hasher: inout Swift.Hasher)
  public init?(rawValue: HealthCardAccess.ApplicationIdentifier)
  public typealias RawValue = HealthCardAccess.ApplicationIdentifier
  public var hashValue: Swift.Int {
    get
  }
  public var rawValue: HealthCardAccess.ApplicationIdentifier {
    get
  }
}
extension CardChannelType {
  public func readCardType(cardAid: HealthCardControl.CardAid? = nil, writeTimeout: Foundation.TimeInterval = 30.0, readTimeout: Foundation.TimeInterval = 30.0) -> Combine.AnyPublisher<HealthCardAccess.HealthCardPropertyType, Swift.Error>
}
extension CardType {
  public func openSecureSession(can: HealthCardAccess.CAN, writeTimeout: Foundation.TimeInterval = 30, readTimeout: Foundation.TimeInterval = 30) -> Combine.AnyPublisher<HealthCardControl.SecureHealthCardType, Swift.Error>
}
extension HealthCard {
  public enum Error : Swift.Error {
    case operational
    case unexpectedResponse(actual: HealthCardAccess.ResponseStatus, expected: HealthCardAccess.ResponseStatus)
    case unknownCardType(aid: HealthCardAccess.ApplicationIdentifier?)
    case illegalGeneration(version: HealthCardAccess.CardVersion2)
    case unsupportedCardType
  }
}
public typealias AuthenticationResult = (certificate: HealthCardAccess.CertificateInfo, signature: HealthCardAccess.Signature)
extension HealthCardType {
  public func authenticate(challenge: Foundation.Data) -> Combine.AnyPublisher<HealthCardControl.AuthenticationResult, Swift.Error>
}
public enum AutCertInfo {
  case efAutR2048
  case efAutE256
  public var eSign: HealthCardAccess.ApplicationIdentifier {
    get
  }
  public var certificate: HealthCardAccess.DedicatedFile {
    get
  }
  public var algorithm: HealthCardAccess.PSOAlgorithm {
    get
  }
  public var key: HealthCardAccess.Key {
    get
  }
  public static func == (a: HealthCardControl.AutCertInfo, b: HealthCardControl.AutCertInfo) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public typealias AutCertificateResponse = (info: HealthCardControl.AutCertInfo, certificate: Foundation.Data)
extension HealthCardType {
  public func readAutCertificate() -> Combine.AnyPublisher<HealthCardControl.AutCertificateResponse, Swift.Error>
}
extension HealthCardPropertyType {
  public var autCertInfo: HealthCardControl.AutCertInfo? {
    get
  }
}
extension AutCertInfo {
  public var signatureHashMethod: (Foundation.Data) -> Foundation.Data {
    get
  }
}
extension HealthCardType {
  public func sign(data: Foundation.Data, hasher: @escaping (Foundation.Data, HealthCardControl.AutCertInfo) -> Foundation.Data = { data, cert in cert.signatureHashMethod(data) }) -> Combine.AnyPublisher<HealthCardAccess.HealthCardResponseType, Swift.Error>
}
public enum ReadError : Swift.Error, Swift.Equatable {
  case unexpectedResponse(state: HealthCardAccess.ResponseStatus)
  case noData(state: HealthCardAccess.ResponseStatus)
  case fcpMissingReadSize(state: HealthCardAccess.ResponseStatus)
  public static func == (a: HealthCardControl.ReadError, b: HealthCardControl.ReadError) -> Swift.Bool
}
public enum SelectError : Swift.Error, Swift.Equatable {
  case failedToSelectAid(HealthCardAccess.ApplicationIdentifier, status: HealthCardAccess.ResponseStatus?)
  case failedToSelectFid(HealthCardAccess.FileIdentifier, status: HealthCardAccess.ResponseStatus?)
  public static func == (a: HealthCardControl.SelectError, b: HealthCardControl.SelectError) -> Swift.Bool
}
extension HealthCardType {
  public func readSelectedFile(expected size: Swift.Int?, failOnEndOfFileWarning: Swift.Bool = true, offset: Swift.Int = 0) -> Combine.AnyPublisher<Foundation.Data, Swift.Error>
  public func selectDedicated(file: HealthCardAccess.DedicatedFile, fcp: Swift.Bool = false, length: Swift.Int = 256) -> Combine.AnyPublisher<(HealthCardAccess.ResponseStatus, HealthCardAccess.FileControlParameter?), Swift.Error>
}
public enum VerifyPinResponse {
  case success
  case failed(retryCount: Swift.Int)
}
extension VerifyPinResponse : Swift.Equatable {
  public static func == (a: HealthCardControl.VerifyPinResponse, b: HealthCardControl.VerifyPinResponse) -> Swift.Bool
}
extension HealthCardType {
  public func verify(pin: HealthCardAccess.Format2Pin, type: HealthCardAccess.EgkFileSystem.Pin) -> Combine.AnyPublisher<HealthCardControl.VerifyPinResponse, Swift.Error>
}
@_hasMissingDesignatedInitializers public class KeyAgreement {
  public enum Error : Swift.Error, Swift.Equatable {
    case illegalArgument
    case unexpectedFormedAnswerFromCard
    case resultOfEcArithmeticWasInfinite
    case macPcdVerificationFailedOnCard
    case macPiccVerificationFailedLocally
    case noValidHealthCardStatus
    case efCardAccessNotAvailable
    case unsupportedKeyAgreementAlgorithm(ASN1Kit.ObjectIdentifier)
    public static func == (a: HealthCardControl.KeyAgreement.Error, b: HealthCardControl.KeyAgreement.Error) -> Swift.Bool
  }
  public enum Algorithm {
    case idPaceEcdhGmAesCbcCmac128
    public func negotiateSessionKey(card: HealthCardAccess.HealthCardType, can: HealthCardAccess.CAN, writeTimeout: Foundation.TimeInterval = 10, readTimeout: Foundation.TimeInterval = 10) -> Combine.AnyPublisher<HealthCardControl.SecureMessaging, Swift.Error>
    public static func == (a: HealthCardControl.KeyAgreement.Algorithm, b: HealthCardControl.KeyAgreement.Algorithm) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  @objc deinit
}
@_hasMissingDesignatedInitializers public class KeyDerivationFunction {
  public enum KeyFuncType {
    case aes128
    public static func == (a: HealthCardControl.KeyDerivationFunction.KeyFuncType, b: HealthCardControl.KeyDerivationFunction.KeyFuncType) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum Mode {
    case enc
    case mac
    case password
    public static func == (a: HealthCardControl.KeyDerivationFunction.Mode, b: HealthCardControl.KeyDerivationFunction.Mode) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public static func deriveKey(from sharedSecret: Foundation.Data, funcType: HealthCardControl.KeyDerivationFunction.KeyFuncType = .aes128, mode: HealthCardControl.KeyDerivationFunction.Mode) -> Foundation.Data
  @objc deinit
}
public protocol SecureHealthCardType : HealthCardAccess.HealthCardType {
}
public protocol SecureMessaging {
  func encrypt(command: CardReaderProviderApi.CommandType) throws -> CardReaderProviderApi.CommandType
  func decrypt(response: CardReaderProviderApi.ResponseType) throws -> CardReaderProviderApi.ResponseType
  func invalidate()
}
